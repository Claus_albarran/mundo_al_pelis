<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Content extends Model
{
    protected $table = 'content';

    /*funcion que permite hacer la union entre la tabla/modelo //qualification 
    */ 
    public function Qualifications()
    {
    	return $this->HasMany('App\Qualification');
    }
}
