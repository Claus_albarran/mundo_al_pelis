<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Content;

class ContentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $media = \App\Content::all();
        return view('index')->with(array(
            "media" => $media
        ));        
    }

    public function show($id)
    {
        //
    }

}
