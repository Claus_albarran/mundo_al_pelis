<?php

use Illuminate\Database\Seeder;
use App\Content;

class ContentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //DB::table('content')->truncate();

        Content::create(array(
        	'type' => 'pelicula',
        	'img' => 'avangers.jfif',
        	'name' => 'Avangers Infinity War',
        	'synopsis' => 'El todopoderoso Thanos ha despertado con la promesa de arrasar con todo a su paso, portando el Guantelete del Infinito, que le confiere un poder incalculable. Los únicos capaces de pararle los pies son los Vengadores y el resto de superhéroes de la galaxia, que deberán estar dispuestos a sacrificarlo todo por un bien mayor. Capitán América e Ironman deberán limar sus diferencias, Black Panther apoyará con sus tropas desde Wakanda, Thor y los Guardianes de la Galaxia e incluso Spider-Man se unirán antes de que los planes de devastación y ruina pongan fin al universo. ¿Serán capaces de frenar el avance del titán del caos?',
        	'gender' => 'Acción',
        	'publish_date' => '2018-04-23'
        ));
        Content::create(array(
        	'type' => 'pelicula',
        	'img' => 'outbreak.jfif',
        	'name' => 'Outbreak',
        	'synopsis' => 'En 1967 Billy Ford (Morgan Freeman) descubre en Zaire un virus llamado "motaba" con un índice de mortalidad del 100%, al afectar a las veinticuatro horas de la infección. El gobierno de EE.UU. supone que bombardeando el poblado infectado destruirán el único brote.',
        	'gender' => 'drama',
        	'publish_date' => '1995-03-10'
        ));
        Content::create(array(
        	'type' => 'pelicula',
        	'img' => 'jhonny.jpg',
        	'name' => 'Johnny English',
        	'synopsis' => 'Johnny English está de vuelta. Esta vez, el más peculiar agente del Servicio de Inteligencia británico deberá enfrentarse a su misión más arriesgada: capturar al responsable de un ciberataque que ha desvelado la identidad de todos los agentes secretos del país. Johnny English se convertirá en la única esperanza para encontrar al hacker. Claro que el verdadero reto será que English tendrá que esforzarse para superar todos los desafíos tecnológicos de la era moderna. ',
        	'gender' => 'comedia',
        	'publish_date' => '2003-04-06'
        ));
        Content::create(array(
        	'type' => 'pelicula',
        	'img' => 'halloween.jpg',
        	'name' => 'Halloween',
        	'synopsis' => 'Jamie Lee Curtis regresa a su icónico personaje Laurie Strode, quien llega a la confrontación final con Michael Myers, la figura enmascarada que la ha perseguido desde que escapó de la matanza que él cometió la noche de Halloween de hace cuatro décadas. Nueva entrega de "Halloween", secuela directa de la original de 1978. John Carpenter (creador de la original) es el productor ejecutivo.',
        	'gender' => 'terror',
        	'publish_date' => '2018-10-17'
        ));
        Content::create(array(
        	'type' => 'pelicula',
        	'img' => 'vinci.jpg',
        	'name' => 'The Da Vinci Code',
        	'synopsis' => 'El catedrático y afamado simbologista Robert Langdon (Tom Hanks) se ve obligado a acudir una noche al Museo del Louvre, cuando el asesinato de un restaurador deja tras de sí un misterioso rastro de símbolos y pistas. Con la ayuda de la criptógrafa de la policía Sophie Neveu (Audrey Tautou) y poniendo en juego su propia vida, Langdon descubre que la obra de Leonardo Da Vinci esconde una serie de misterios que apuntan a una sociedad secreta encargada de custodiar un antiguo secreto que ha permanecido oculto durante dos mil años..',
        	'gender' => 'drama',
        	'publish_date' => '2006-05-18'
        ));
        Content::create(array(
        	'type' => 'pelicula',
        	'img' => 'bohemian.jpg',
        	'name' => 'Bohemian Rhapsody',
        	'synopsis' => 'Bohemian Rhapsody es una celebración del grupo Queen, de su música y de su singular cantante Freddie Mercury, que desafió estereotipos para convertirse en uno de los showmans más queridos del mundo. El film plasma el meteórico ascenso de la banda al olimpo de la música a través de sus icónicas canciones y su revolucionario sonido, su crisis cuando el estilo de vida de Mercury estuvo fuera de control, y su triunfal reunión en la víspera del Live Aid, en la que Mercury, mientras sufría una enfermedad que amenazaba su vida, lidera a la banda en uno de los conciertos de rock más grandes de la historia. Refleja asimismo cómo se cimentó el legado de una banda que siempre se pareció más a una familia, y que continúa inspirando soñadores y amantes de la música hasta nuestros días',
        	'gender' => 'drama',
        	'publish_date' => '2018-10-24'
        ));
        Content::create(array(
        	'type' => 'serie',
        	'img' => 'park.jpg',
        	'name' => 'Dr House',
        	'synopsis' => 'House (o House M.D., donde M.D. corresponde a un acrónimo de “Medicine Doctor”) es una serie semanal donde se muestra la vida de un doctor y lo que lo rodea en un Hospital Universitario en Nueva Jersey.
			Hugh Laurie es quien encarna el papel del famoso Dr. Gregory House, un complicado médico especialista en enfermedades infecciosas el cual se rehusa a visitar a sus enfermos porque, como siempre repite, “Everybody lies” (todos mienten), e intenta buscar respuestas por otros métodos poco ortodoxos.',
			'gender' => 'drama',
        	'publish_date' => '2004-11-16'
        ));
        Content::create(array(
        	'type' => 'serie',
        	'img' => 'park.jpg',
        	'name' => 'South Park',
        	'synopsis' => 'South Park gira en torno a Kyle, Cartman, Stan y Kenny que son cuatro amigos groseros que crecen en un pueblo surrealista llamado South Park en Colorado, un pueblo cuyos ciudadanos parecen ser más políticamente correctos que el resto.',
        	'gender' => 'comedia',
        	'publish_date' => '1997-08-13'
        ));
    }
}
