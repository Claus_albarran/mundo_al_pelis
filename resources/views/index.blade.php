@extends('layouts.master')

@section('content')
<section class="content-header">
  <h1>Peliculas recientemente agregadas</h1>
</section>
<hr>
  @foreach($media as $peliculas)
    <!--  Tarjeta para cada pelicula-->
    <div class="col-lg-4 col-xs-7">
      <div class="small-box bg-light-blue-gradient">
        <div class="inner">
          <img src="{{ asset('/dist/img/'.$peliculas->img) }}" class="img-responsive" style="max-height: 70%; max-width: 100%; min-width: 70%; min-width: 100%">
          <h2>{{$peliculas->name}}</h2>
          <h3>{{$peliculas->gender}}</h3>
        </div>
      </div>
    </div>
  @endforeach


@endsection