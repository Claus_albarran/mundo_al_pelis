<footer class="main-footer no-print">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.0
    </div>
    <strong>Copyright &copy; 2020 <a href="https://gitlab.com/Claus_albarran">Nicolas Albarran</a>.</strong> All rights
    reserved.
  </footer>