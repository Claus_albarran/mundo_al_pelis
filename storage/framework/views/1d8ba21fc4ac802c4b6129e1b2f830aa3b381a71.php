 <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <form action="/content" method="get" class="sidebar-form" id="form-filtros">
      <ul class="sidebar-menu" data-widget="tree">
        <!-- search form -->
        
          <input type="text" name="search" class="form-control" placeholder="Search...">
        
        <li class="header">PELICULAS</li>
        <li>
          <a href="">
            <span>Acción</span>
          </a>
        </li>
        <li>
          <a href="">
            <span>Drama</span>
          </a>
        </li>
        <li>
          <a href="">
            <span>Comedia</span>
          </a>
        </li>
        <li>
          <a href="">
            <span>Terror</span>
          </a>
        </li>
        <li>
          <a href="">
            <span>Thriler</span>
          </a>
        </li>
        <li class="header">SERIES</li>
        <li>
          <a href="">
            <span>Acción</span>
          </a>
        </li>
        <li>
          <a href="">
            <span>Drama</span>
          </a>
        </li>
        <li>
          <a href="">
            <span>Comedia</span>
          </a>
        </li>
        <li>
          <a href="">
            <span>Terror</span>
          </a>
        </li>
        <li>
          <a href="">
            <span>Thriler</span>
          </a>
        </li>
      </ul>
      </form>
    </section>
    <!-- /.sidebar -->
  </aside>
  <?php $__env->startSection('scripts'); ?>
  ##parent-placeholder-16728d18790deb58b3b8c1df74f06e536b532695##
    <script type="text/javascript">
      $(document).ready(function() {
        $('#tipo_filtros').change(function(){
          $('#form-filtros').submit();
        });
      });
    </script>
  <?php $__env->stopSection(); ?>