<?php $__env->startSection('content'); ?>
<section class="content-header">
  <h1>Peliculas recientemente agregadas</h1>
</section>
<hr>
  <?php $__currentLoopData = $media; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $peliculas): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <!--  Tarjeta para cada pelicula-->
    <div class="col-lg-4 col-xs-7">
      <div class="small-box bg-light-blue-gradient">
        <div class="inner">
          <img src="<?php echo e(asset('/dist/img/'.$peliculas->img)); ?>" class="img-responsive" style="max-height: 70%; max-width: 100%; min-width: 70%; min-width: 100%">
          <h2><?php echo e($peliculas->name); ?></h2>
          <h3><?php echo e($peliculas->gender); ?></h3>
        </div>
      </div>
    </div>
  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>